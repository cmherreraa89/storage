import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private nativeStorage: NativeStorage,
    private alertCtrl: AlertController,
    private platform: Platform,) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }

  ionViewWillEnter() {  
    let er = this.nativeStorage.setItem('myitem', {property: 'value', anotherProperty: 'anotherValue'})
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );

    let stor = this.nativeStorage.getItem('myitem')
      .then(
        data => console.log(data),
        error => er = error
      );

      let alert = this.alertCtrl.create({ // estructura mensaje de alerta
        title: 'Atención',
        subTitle: ' Cordova  El usuario es: ' + stor + er,
        buttons: ['Aceptar']
      });
      alert.present();


      this.platform.ready().then(() => {
        this.nativeStorage.setItem('myitem', {property: 'value', anotherProperty: 'anotherValue'})
        .then(
          () => console.log('Stored item!'),
          error => 
         
          console.error('Error storing item', error)
        ); });

  }



}
